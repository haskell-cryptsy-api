{-# LANGUAGE EmptyDataDecls, GeneralizedNewtypeDeriving #-}
-- |Default numeric type for prices, quanities, and total returned from the
-- API.
module Cryptsy.API.Public.Types.Num where

-- aeson
import Data.Aeson (FromJSON(..))

-- base
import Data.Fixed (HasResolution(..), Fixed)
import Numeric (readFloat)

-- this package
import Cryptsy.API.Public.Types.Internal

-- |Custom resolution for 'Fixed'; 1e8.
data E8

instance HasResolution E8 where
	resolution = const 100000000

-- |Native number type for the API, newtype for custom FromJSON instance, which
-- only accepts JSON strings.
newtype CryptsyNum = CryptsyNum { toFixed :: Fixed E8 } deriving
	(Eq, Ord, Show, Read, Num, Real, Fractional, Enum, RealFrac)

instance FromJSON CryptsyNum where
	parseJSON = readsToParser "CrypstyNum" readFloat

