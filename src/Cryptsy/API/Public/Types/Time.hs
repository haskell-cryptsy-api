{-# LANGUAGE GeneralizedNewtypeDeriving #-}
-- |Default type for times returned by the API.
module Cryptsy.API.Public.Types.Time where

-- aeson
import Data.Aeson (FromJSON(..))

-- deepseq
import Control.DeepSeq (NFData)

-- old-locale
import System.Locale (defaultTimeLocale)

-- time
import Data.Time.Format (FormatTime, ParseTime, readsTime)
import Data.Time.LocalTime (LocalTime)

-- this package
import Cryptsy.API.Public.Types.Internal

-- |Native date/time type for the API, newtype for custom FromJSON instance,
-- which only accepts JSON strings
newtype CryptsyTime = CryptsyTime { toLocalTime :: LocalTime } deriving
	(Eq, Ord, Show, Read, NFData, ParseTime, FormatTime)

instance FromJSON CryptsyTime where
	parseJSON = readsToParser "CryptsyTime"
	          $ readsTime defaultTimeLocale "%F %T"
