-- |Sum type for any error conditions in a API request.
module Cryptsy.API.Public.Types.Error where

-- aeson
import Data.Aeson (Value)

-- http-client
import Network.HTTP.Client (HttpException, Request)

-- pipes-attoparsec
import Pipes.Attoparsec (ParsingError)

-- |error conditions w/ debugging information for an API request
data CryptsyError = BadURL { badURL :: String, httpException :: HttpException }
                  | FailReadResponse { request :: Request, httpException :: HttpException }
                  | FailParseResponse { parsingError :: ParsingError }
                  | ErrorResponse { errorValue :: Value }
                  | UnsuccessfulResponse { jsonResponse :: Value }
                  | FailParseReturn { dataValue :: Value, errorMessage :: String }
                  deriving Show
