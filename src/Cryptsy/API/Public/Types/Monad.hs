-- |Monad in which public API requests occur.
module Cryptsy.API.Public.Types.Monad
	( module Cryptsy.API.Public.Types.Monad
	, module Cryptsy.API.Public.Types.Error
	)
where

-- either
import Control.Monad.Trans.Either (EitherT)

-- http-client
import Network.HTTP.Client (CookieJar, Manager)

-- transformers
import Control.Monad.Trans.Reader (ReaderT)
import Control.Monad.Trans.State (StateT)

-- this package
import Cryptsy.API.Public.Types.Error

-- |request monad
type PubCryptsy = ReaderT Manager (EitherT CryptsyError (StateT (Maybe CookieJar) IO))
