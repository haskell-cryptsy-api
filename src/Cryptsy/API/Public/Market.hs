{-# LANGUAGE FlexibleContexts, ViewPatterns #-}
-- |Request for a single market by market id.
module Cryptsy.API.Public.Market
	( module Cryptsy.API.Public.Market
	, module Cryptsy.API.Public.Types.Market
	, module Cryptsy.API.Public.Types.Monad
	)
where

-- text
import Data.Text (Text, unpack)

-- aeson
import Data.Aeson (FromJSON(..), withObject)

-- unordered-containers
import Data.HashMap.Strict (toList)

-- this package
import Cryptsy.API.Public.Internal
import Cryptsy.API.Public.Types.Market
import Cryptsy.API.Public.Types.Monad

-- |single market API request
singleMarket :: FromJSON (GMarket p q dt t)
             => Text -- ^ marketid
             -> PubCryptsy (GMarket p q dt t)
singleMarket (unpack -> reqMarket) = pubCryptsy marketURL $ onMarkets parseSingleMarket
 where
	marketURL = pubURL $ "singlemarketdata&marketid=" ++ reqMarket
	parseSingleMarket = withObject dataStr $ \o ->
		case toList o of
			[]       -> fail "No market returned."
			[(_, v)] -> parseJSON v
			_        -> fail "Multiple markets returned."
{-# INLINABLE singleMarket #-}
