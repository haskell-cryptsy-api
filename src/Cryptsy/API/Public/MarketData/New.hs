{-# LANGUAGE FlexibleContexts #-}
-- |Request for new-style market data, without ambiguous keys
module Cryptsy.API.Public.MarketData.New
	( marketData
	, module Cryptsy.API.Public.Types.MarketData
	, module Cryptsy.API.Public.Types.Monad
	)
where

-- aeson
import Data.Aeson (FromJSON(..))

-- this pacakge
import Cryptsy.API.Public.Internal
import Cryptsy.API.Public.Types.MarketData
import Cryptsy.API.Public.Types.Monad

marketdatav2URL :: String
marketdatav2URL = pubURL "marketdatav2"

-- |Request all markets.
marketData :: FromJSON (GMarketData p q dt t)
           => PubCryptsy (GMarketData p q dt t)
marketData = pubCryptsy marketdatav2URL $ onMarkets parseJSON
{-# INLINABLE marketData #-}
