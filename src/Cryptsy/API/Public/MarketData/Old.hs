{-# LANGUAGE FlexibleContexts #-}
-- |Request for market data in old format, with ambiguous keys
module Cryptsy.API.Public.MarketData.Old
	( oldMarketData
	, module Cryptsy.API.Public.Types.MarketData
	, module Cryptsy.API.Public.Types.Monad
	)
where

-- aeson
import Data.Aeson (FromJSON(..))

-- this pacakge
import Cryptsy.API.Public.Internal
import Cryptsy.API.Public.Types.MarketData
import Cryptsy.API.Public.Types.Monad

marketdataURL :: String
marketdataURL = pubURL "marketdata"

-- |Request all markets in old format.
oldMarketData :: FromJSON (GMarketData p q dt t)
           => PubCryptsy (GMarketData p q dt t)
oldMarketData = pubCryptsy marketdataURL $ onMarkets parseJSON
{-# INLINABLE oldMarketData #-}
