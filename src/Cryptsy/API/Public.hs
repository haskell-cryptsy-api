-- |Re-exports the entire Cryptsy.API.Public.* heirarchy.
module Cryptsy.API.Public
	( module Cryptsy.API.Public.Types
	, module Cryptsy.API.Public.MarketData.Old
	, module Cryptsy.API.Public.MarketData.New
	, module Cryptsy.API.Public.Market
	, module Cryptsy.API.Public.OrderData
	, module Cryptsy.API.Public.OrderBook
	, defaultEvalPubCryptsy
	, defaultRunPubCryptsy
	)
where

-- base
import Data.Maybe (fromMaybe)
import Data.Monoid (mempty)

-- either
import Control.Monad.Trans.Either (runEitherT)

-- http-client
import Network.HTTP.Client (CookieJar, withManager)

-- http-client-tls
import Network.HTTP.Client.TLS (tlsManagerSettings)

-- transformers
import Control.Monad.Trans.Reader (runReaderT)
import Control.Monad.Trans.State (runStateT)

-- this package
import Cryptsy.API.Public.Market
import Cryptsy.API.Public.MarketData.New
import Cryptsy.API.Public.MarketData.Old
import Cryptsy.API.Public.OrderBook
import Cryptsy.API.Public.OrderData
import Cryptsy.API.Public.Types

{-|
Like 'defaultRunPubCryptsy', but the resulting cookir jar is ignored.
-}
defaultEvalPubCryptsy :: PubCryptsy a -> IO (Either CryptsyError a)
defaultEvalPubCryptsy = fmap fst . defaultRunPubCryptsy
{-# INLINABLE defaultEvalPubCryptsy #-}

{-|
Converts a public cryptsy action to an action in the IO Monad, using a new
manager created from 'tlsManagerSettings' and closed as the last part of the IO
action and starting from an empty cookie jar.  Both the resulting cookie jar
and either the resolt of the public cryptsy action or the first cryptsy error
encountered are the results of the IO action.
-}
defaultRunPubCryptsy :: PubCryptsy a -> IO (Either CryptsyError a, CookieJar)
defaultRunPubCryptsy = withManager tlsManagerSettings
                     . (defaultRunInReader .) . runReaderT
 where
	defaultRunInReader = fmap (msnd (fromMaybe mempty)) . ($ Just mempty)
	                   . runStateT . runEitherT
	msnd f (x, y) = (x, f y)
{-# INLINABLE defaultRunPubCryptsy #-}
