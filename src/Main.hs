module Main (main) where

-- base
import Control.Monad ((<=<))

-- text
import Data.Text (pack)

-- transformers
import Control.Monad.IO.Class (liftIO)

-- unordered-containers
import qualified Data.HashMap.Strict as HM

-- vector
import qualified Data.Vector as V

-- this package
import Cryptsy.API.Public

main :: IO ()
main = either print return <=< defaultEvalPubCryptsy $ do
	books <- orderData
	liftIO . print . HM.size $ orderBooks (books :: OrderData)
	omkts <- oldMarketData
	liftIO . print . HM.size $ markets (omkts :: MarketData)
	mkts <- marketData
	liftIO . print . HM.size $ markets (mkts :: MarketData)
	book <- singleOrderBook $ pack "113"
	liftIO . print $ ( V.length $ obBuyOrders (book :: OrderBook)
	                 , V.length $ obSellOrders book
	                 )
	market <- singleMarket $ pack "113"
	liftIO . print $ ( V.length $ mktRecentTrades (market :: Market)
	                 , V.length $ mktBuyOrders market
	                 , V.length $ mktSellOrders market
	                 )
